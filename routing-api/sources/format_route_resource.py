import os
from google.cloud.compute_v1 import types

def format_route_resource(vpc, dest_range, source_ip):
	#Funcion que genera la clase route_resource 
	route_priority = os.environ["ROUTE_API_ROUTE_PRIORITY"]
	params={
						"dest_range": dest_range,
						"name": "routing-api--route-"+dest_range.replace(".","-").replace("/","-")+"-nexthop-"+source_ip.replace(".","-"),
						"network": vpc,
						"priority": int(route_priority),
						"tags": [],
						"next_hop_ip": source_ip
					}
	route_resource = types.compute.Route(params)
	return route_resource