#!/bin/dash
#Script de incialización de Servidor Strongswan

#Configuración de Variables de Entorno
echo REBA_GCP_SHARED_SERVICES_PROJECT=shared-services-vpc-319119 >> /etc/environment
export REBA_GCP_SHARED_SERVICES_PROJECT=shared-services-vpc-319119
echo REBA_GCP_SHARED_SERVICES_NETWORK=projects/shared-services-vpc-319119/global/networks/vpc-shared-services >> /etc/environment
export REBA_GCP_SHARED_SERVICES_NETWORK=projects/shared-services-vpc-319119/global/networks/vpc-shared-services
echo ROUTE_API_ROUTE_PRIORITY=10 >> /etc/environment
export ROUTE_API_ROUTE_PRIORITY=10
echo REBA_GCP_SHARED_SERVICES_REGION=us-west1 >> /etc/environment
export REBA_GCP_SHARED_SERVICES_REGION=us-west1
echo REBA_GCP_SHARED_SERVICES_ROUTER=vpc-shared-services-cloud-router >> /etc/environment
export REBA_GCP_SHARED_SERVICES_ROUTER=vpc-shared-services-cloud-router

#Instalacion de dependencias
apt-get install strongswan python3 python3-pip apparmor-utils -y
echo $(date -u) "Instalados paquetes" >> /var/log/syslog
pip3 install testresources flask==2.0.0 google-cloud-compute==0.4.2 gevent==21.8.0
echo $(date -u) "Instaladas librerias de Python" >> /var/log/syslog
cp ./init_config/starter.conf /etc/strongswan.d/starter.conf
echo $(date -u) "starter.conf copiado" >> /var/log/syslog
cp ./init_config/stroke.conf /etc/strongswan.d/charon/stroke.conf
echo $(date -u) "stroke.conf copiado" >> /var/log/syslog

#Habilitación de IP Routing
sysctl -w net.ipv4.ip_forward=1
sysctl -p

#Reemplazo de direcciones IP en VPN HA
if [ "$STRONGSWAN_HA_SERVER" = "primary"  ]
then
	STRONGSWAN_LOCAL_IP="10.67.224.10"
	STRONGSWAN_REMOTE_IP="10.67.224.20"
    STRONGSWAN_LOCAL_ENCRYPTION_DOMAIN="10.67.239.253"
    STRONGSWAN_REMOTE_ENCRYPTION_DOMAIN="10.67.239.254"

else
	STRONGSWAN_LOCAL_IP="10.67.224.20"
	STRONGSWAN_REMOTE_IP="10.67.224.10"
    STRONGSWAN_LOCAL_ENCRYPTION_DOMAIN="10.67.239.254"
    STRONGSWAN_REMOTE_ENCRYPTION_DOMAIN="10.67.239.253"
fi
echo $(date -u) " STRONGSWAN_LOCAL_IP: " $STRONGSWAN_LOCAL_IP " STRONGSWAN_REMOTE_IP: "$STRONGSWAN_REMOTE_IP >> /var/log/syslog
echo $(date -u) " STRONGSWAN_LOCAL_ENCRYPTION_DOMAIN: " $STRONGSWAN_LOCAL_ENCRYPTION_DOMAIN " STRONGSWAN_REMOTE_ENCRYPTION_DOMAIN: "$STRONGSWAN_REMOTE_ENCRYPTION_DOMAIN >> /var/log/syslog
sed -e 's|STRONGSWAN_LOCAL_IP|'$STRONGSWAN_LOCAL_IP'|g' -e 's|STRONGSWAN_REMOTE_IP|'$STRONGSWAN_REMOTE_IP'|g' -e 's|STRONGSWAN_LOCAL_ENCRYPTION_DOMAIN|'$STRONGSWAN_LOCAL_ENCRYPTION_DOMAIN'|g' -e 's|STRONGSWAN_REMOTE_ENCRYPTION_DOMAIN|'$STRONGSWAN_REMOTE_ENCRYPTION_DOMAIN'|g' -i /opt/app/ipsec-vpn-server/vpn_tunnels/vpn-server-ha.conf
echo $(date -u) "Reemplazadas IPs en vpn-server-ha.conf" >> /var/log/syslog
sed -e 's|STRONGSWAN_LOCAL_IP|'$STRONGSWAN_LOCAL_IP'|g' -e 's|STRONGSWAN_REMOTE_IP|'$STRONGSWAN_REMOTE_IP'|g'  -i /opt/app/ipsec-vpn-server/ipsec.secrets
echo $(date -u) "Reemplazadas IPs en ipsec.secrets" >> /var/log/syslog

iptables -t nat -A PREROUTING -s $STRONGSWAN_REMOTE_ENCRYPTION_DOMAIN/32 -d $STRONGSWAN_LOCAL_ENCRYPTION_DOMAIN/32 -j DNAT --to-destination $STRONGSWAN_LOCAL_IP
iptables -t nat -A POSTROUTING -s $STRONGSWAN_LOCAL_IP/32 -d $STRONGSWAN_REMOTE_ENCRYPTION_DOMAIN/32 -j SNAT --to-source $STRONGSWAN_LOCAL_ENCRYPTION_DOMAIN
iptables-save
echo $(date -u) "Reglas de iptables vpn-server-ha configuradas" >> /var/log/syslog
#Settear AppArmor en modo Complain para Servicios de Strongswan
aa-complain /usr/lib/ipsec/charon
aa-complain /usr/lib/ipsec/stroke
echo '/bin/bash rmPUx,' | sudo tee -a /etc/apparmor.d/local/usr.lib.ipsec.charon
apparmor_parser -rTW /etc/apparmor.d/usr.lib.ipsec.charon
echo $(date -u) "Configurado AppArmor" >> /var/log/syslog
ipsec restart 
echo $(date -u) "Servicio reinciado" >> /var/log/syslog