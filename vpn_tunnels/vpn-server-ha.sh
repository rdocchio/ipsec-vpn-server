#!/bin/bash

# set charon.install_virtual_ip = no to prevent the daemon from also installing the VIP
# INSERT: curl "http://localhost:5000/gcp_route_update?dest_range=8.8.8.8/32&action=insert"
# DELETE: curl "http://localhost:5000/gcp_route_update?dest_range=8.8.8.8/32&action=delete"

set -o nounset
set -o errexit

#Listado de rutas que se instalarán para el túnel
VTI_ROUTES=(
	10.67.239.252/30
)
#Metrica de las rutas que se instalaran
VTI_ROUTE_METRIC=100
#Nombre de interfaz VTI
VTI_IF="vti${PLUTO_UNIQUEID}"

case "${PLUTO_VERB}" in
	up-client)
	#Si el túnel se estableció se crea la interfaz tunel con el nombre y mark elegido
		ip tunnel add "${VTI_IF}" local "${PLUTO_ME}" remote "${PLUTO_PEER}" mode vti key "${PLUTO_MARK_OUT%%/*}"
		ip link set "${VTI_IF}" up
		#La linea de abajo va comentada si no se utilizan Ips en la interfaz tunel
		#ip addr add "${PLUTO_MY_SOURCEIP}" dev "${VTI_IF}"
		#Para cada elemento del array VTI_ROUTES se crea una ruta
		for i in "${VTI_ROUTES[@]}"; do
			ip route add "${i}" dev "${VTI_IF}" metric "${VTI_ROUTE_METRIC}"
			#Propagar la ruta a GCP
			curl "http://localhost:5000/gcp_route_update?dest_range=${i}&action=insert"
		done
		#Se deshabilita policy Routing
		sysctl -w "net.ipv4.conf.${VTI_IF}.disable_policy=1"
		;;
	down-client)
		#Si la fase 2 se cae, se elimina la interfaz VTI creada
		ip tunnel del "${VTI_IF}"
		for i in "${VTI_ROUTES[@]}"; do
			#Eliminar la ruta propagada
			curl "http://localhost:5000/gcp_route_update?dest_range=${i}&action=delete"
		done
		
		;;
esac