import os

def get_advertised_ip(router_client):
    project_id = os.environ["REBA_GCP_SHARED_SERVICES_PROJECT"]
    region=os.environ["REBA_GCP_SHARED_SERVICES_REGION"]
    vpc_router= os.environ["REBA_GCP_SHARED_SERVICES_ROUTER"]
    router_advertised_ip=router_client.get(project=project_id,region=region,router=vpc_router).bgp.advertised_ip_ranges
    advertised_ip= []
    for line in str(router_advertised_ip).split("\n"):
        if "range_:" in line:
            advertised_ip.append(line.split('"')[1])
    return advertised_ip