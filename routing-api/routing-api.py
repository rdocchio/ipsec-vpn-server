#!/bin/python3
#Documentación de clase Routes https://googleapis.dev/python/compute/latest/compute_v1/routes.html
#Documentación de clase Routers https://googleapis.dev/python/compute/latest/compute_v1/routers.html
#Documentacion de clase Types https://googleapis.dev/python/compute/latest/compute_v1/types.html#google.cloud.compute_v1.types.Route
#
# Query con cURL para actualización de rutas:
# INSERT: curl "http://localhost:5000/gcp_route_update?dest_range=8.8.8.8/32&action=insert"
# DELETE: curl "http://localhost:5000/gcp_route_update?dest_range=8.8.8.8/32&action=delete"


import os
from flask import Flask, request
from gevent.pywsgi import WSGIServer
from google.cloud.compute_v1 import services
from sources.format_route_resource import format_route_resource
from sources.format_router_resource import format_router_resource
from sources.get_advertised_ip import get_advertised_ip

#Creacion de Clase de clientes de GCP
route_client=services.routes.RoutesClient()
router_client=services.routers.RoutersClient()

api = Flask(__name__)

@api.route('/gcp_route_update', methods=['GET'])
def gcp_route_update():
	#Obtencion de Project id
	project_id = os.environ["REBA_GCP_SHARED_SERVICES_PROJECT"]
	#Obtencion de Network de GCP
	vpc = os.environ["REBA_GCP_SHARED_SERVICES_NETWORK"]
	#Obtencion del Router de GCP
	vpc_router = os.environ["REBA_GCP_SHARED_SERVICES_ROUTER"]
	#Obtencion de Region de GCP
	region = os.environ["REBA_GCP_SHARED_SERVICES_REGION"]
	#Toma los parametros de la consulta HTTP
	action=request.args.get('action', default="null", type=str)
	dest_range=request.args.get('dest_range', default="null", type=str)
	#Obtiene la direccion IP del servidor
	source_ip=os.popen("hostname -i | awk '{print $1}'").read().split("\n")[0]
	
	if action=="null" or dest_range=="null":
		return "Parametros recibidos vacios"
	else:
		if action=="insert":
			route_resource=format_route_resource(vpc, dest_range, source_ip)
			advertised_ip=get_advertised_ip(router_client)
			advertised_ip.append(dest_range)
			patched_router=format_router_resource(advertised_ip)
			router_client.patch(router_resource=patched_router,router=vpc_router,region=region,project=project_id)
			route_client.insert(project=project_id,route_resource=route_resource)
			return "Ruta insertada correctamente, nombre:"+route_resource.name+"\n"
		if action=="delete":
			route_resource=format_route_resource(vpc, dest_range, source_ip)
			advertised_ip=get_advertised_ip(router_client)
			advertised_ip.remove(dest_range)
			patched_router=format_router_resource(advertised_ip)
			router_client.patch(router_resource=patched_router,router=vpc_router,region=region,project=project_id)
			route_client.delete(project=project_id,route=route_resource.name)
			return "Ruta eliminada correctamente, nombre:"+route_resource.name+"\n"


if __name__ == '__main__':
	try:
		#Escucha en todas las IPs del container/server
		http_server = WSGIServer(('', 5000), api)
		http_server.serve_forever()
	except Exception as e: print(e)