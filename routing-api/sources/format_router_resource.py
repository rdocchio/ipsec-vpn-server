from google.cloud.compute_v1 import types

def format_router_resource(advertised_ip):
	#Funcion que genera la clase router_resource
	route_bgp_advertised_ip=[]
	for ip in advertised_ip:
		route_bgp_advertised_ip_element=types.RouterAdvertisedIpRange({"range_" : ip})
		route_bgp_advertised_ip.append(route_bgp_advertised_ip_element)
	router_bgp=types.RouterBgp({"advertised_ip_ranges" : route_bgp_advertised_ip})
	router_resource=types.Router({"bgp" : router_bgp})
	return router_resource